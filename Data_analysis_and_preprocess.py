import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import datetime
from scipy.stats import skew, kurtosis
# Încărcarea setului de date
df = pd.read_csv("Stock_Market_Data.csv")  

# Eliminarea duplicatelor
df = df.drop_duplicates()

# Tratarea valorilor lipsă
df = df.fillna(method='ffill')  

# Verificarea tipurilor de date
print(df.dtypes)

# Transformarea coloanei Date în tip de date datetime
def StrToDateTime(s):
    split = s.split('/')
    monthDf, dayDf, yearDf = int(split[0]), int(split[1]), int(split[2])
    return datetime.datetime(year=yearDf, month=monthDf, day=dayDf)

df['Date'] = df['Date'].apply(StrToDateTime)

# Setarea coloanei Date ca index
df.index = df.pop('Date')

# Afișarea graficului
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['Overall_%Modif'])
plt.title('Evoluția variabilei Overall_%Modif în timp')
plt.xlabel('Data')
plt.ylabel('Overall_%Modif')
plt.xticks(rotation=45)
plt.grid(True)
plt.show()

# Analiza distributiei
plt.hist(df["Overall_%Modif"], bins= 30)
plt.title("Distributia modificarilor preturilor pe piata")
plt.show()

print("Coeficientii de asimetrie si aplatizare:")
print("Asimetrie: ", skew(df['Overall_%Modif']))
print("Aplatizare: ", kurtosis(df['Overall_%Modif']))

# Identificarea outlierelor
plt.boxplot(df["Overall_%Modif"])
plt.title("Identificarea outilerelor in modificarile preturilor")
plt.show()

# Transformarea categoriilor în valori numerice
label_mapping_behaviour = {'Growth': 1, 'Decline': -1, 'Stagnation': 0}
df['Behaviour'] = df['Behaviour'].map(label_mapping_behaviour)


label_mapping_season = {'Winter': 3, 'Spring': 2, 'Summer': 0, 'Autumn': 1}
df['Season'] = df['Season'].map(label_mapping_season)


# Calculăm matricea de corelație
plt.figure(figsize=(12, 8))
sns.heatmap(df.corr(), annot=True, cmap='coolwarm', fmt=".2f", linewidths=0.5)
plt.title('Matricea de corelație a datelor')
plt.show()

# Statistici descriptive
print(df["Overall_%Modif"].describe())


