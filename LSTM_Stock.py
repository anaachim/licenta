import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.metrics import accuracy_score, confusion_matrix, mean_absolute_error
from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout
from keras.utils import to_categorical
from keras.wrappers.scikit_learn import KerasRegressor, KerasClassifier
from sklearn.metrics import mean_squared_error

# Încărcarea setului de date
df = pd.read_csv("Stock_Market_Data.csv")

# Eliminarea duplicatelor
df = df.drop_duplicates()

# Tratarea valorilor lipsă
df = df.fillna(method='ffill')

# Transformarea categoriilor in numere intregi
label_mapping_behaviour = {'Growth': 1, 'Decline': -1, 'Stagnation': 0}
df['Behaviour'] = df['Behaviour'].map(label_mapping_behaviour)

label_mapping_season = {'Winter': 3, 'Spring': 2, 'Summer': 0, 'Autumn': 1}
df['Season'] = df['Season'].map(label_mapping_season)

# Normalizarea datelor numerice
scaler = StandardScaler()
data_scaled = scaler.fit_transform(df.iloc[:, 1:-2])

# Împărțirea setului de date în set de antrenament și set de testare
X_train, X_test, y_train, y_test = train_test_split(data_scaled, df[['Overall_%Modif', 'Behaviour']], test_size=0.2, random_state=42)

# Transformarea etichetelor pentru variabila 'Behaviour'
le = LabelEncoder()
y_train['Behaviour'] = le.fit_transform(y_train['Behaviour'])
y_test['Behaviour'] = le.transform(y_test['Behaviour'])

# Convertirea etichetelor în formă one-hot
y_train_cat = to_categorical(y_train['Behaviour'])
y_test_cat = to_categorical(y_test['Behaviour'])

# Definirea funcției care creează modelul pentru predictia 'Overall_%Modif'
def create_model(units=25, dropout_rate=0.2):
    model = Sequential([
        LSTM(units=units, return_sequences=True, input_shape=(X_train.shape[1], 1)),
        Dropout(dropout_rate),
        LSTM(units=units, return_sequences=True),
        Dropout(dropout_rate),
        LSTM(units=units),
        Dropout(dropout_rate),
        Dense(1, activation='linear')
    ])
    model.compile(optimizer='adam', loss='mean_squared_error', metrics=['mae'])
    return model

# Crearea modelului KerasRegressor
model = KerasRegressor(build_fn=create_model, verbose=0)

# Definirea gridului de parametri 
param_grid = {
    'units': [20, 25, 30],
    'dropout_rate': [0.1, 0.2, 0.3],
    'batch_size': [16, 32, 64],
    'epochs': [50, 100]
}

# Aplicarea GridSearchCV
grid = GridSearchCV(estimator=model, param_grid=param_grid, cv=3, n_jobs=-1)
grid_result = grid.fit(X_train[:, :, np.newaxis], y_train['Overall_%Modif'])

# Afisarea celor mai buni parametri 
print(f"Best parameters: {grid_result.best_params_}")

# Reantrenarea modelului pentru predictia 'Overall_%Modif' cu cei mai buni parametri
best_units = grid_result.best_params_['units']
best_dropout = grid_result.best_params_['dropout_rate']
best_batch_size = grid_result.best_params_['batch_size']
best_epochs = grid_result.best_params_['epochs']

model_overall = Sequential([
    LSTM(units=best_units, return_sequences=True, input_shape=(X_train.shape[1], 1)),
    Dropout(best_dropout),
    LSTM(units=best_units, return_sequences=True),
    Dropout(best_dropout),
    LSTM(units=best_units),
    Dropout(best_dropout),
    Dense(1, activation='linear')
])
model_overall.compile(optimizer='adam', loss='mean_squared_error', metrics=['mae'])
history_overall = model_overall.fit(X_train[:, :, np.newaxis], y_train['Overall_%Modif'], epochs=best_epochs, batch_size=best_batch_size, validation_split=0.2)

# Prezicerea 'Overall_%Modif' pentru seturile de antrenament și testare
y_pred_overall_train = model_overall.predict(X_train[:, :, np.newaxis])
y_pred_overall_test = model_overall.predict(X_test[:, :, np.newaxis])

# Verificarea dimensiunilor pentru concatenare
print(f"Dimensiuni X_train: {X_train.shape}")
print(f"Dimensiuni y_pred_overall_train: {y_pred_overall_train.shape}")

# Verificarea reshaping-ului
if len(y_pred_overall_train.shape) == 1:
    y_pred_overall_train = y_pred_overall_train.reshape(-1, 1)
if len(y_pred_overall_test.shape) == 1:
    y_pred_overall_test = y_pred_overall_test.reshape(-1, 1)

# Verificarea concatenării
X_train_concat = np.concatenate((X_train, y_pred_overall_train), axis=1)
X_test_concat = np.concatenate((X_test, y_pred_overall_test), axis=1)

print(f"Dimensiuni X_train_concat: {X_train_concat.shape}")
print(f"Dimensiuni X_test_concat: {X_test_concat.shape}")

# Definirea funcției care creează modelul pentru clasificarea comportamentului
def create_classification_model(units=25, dropout_rate=0.2):
    model = Sequential([
        LSTM(units=units, return_sequences=True, input_shape=(X_train_concat.shape[1], 1)),
        Dropout(dropout_rate),
        LSTM(units=units, return_sequences=True),
        Dropout(dropout_rate),
        LSTM(units=units),
        Dropout(dropout_rate),
        Dense(3, activation='softmax')
    ])
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    return model

# Crearea modelului KerasClassifier
model_class = KerasClassifier(build_fn=create_classification_model, verbose=0)

# Definirea gridului de parametri 
param_grid_class = {
    'units': [20, 25, 30],
    'dropout_rate': [0.1, 0.2, 0.3],
    'batch_size': [16, 32, 64],
    'epochs': [50, 100, 150]
}

# Aplicarea GridSearchCV pentru clasificarea comportamentului
grid_class = GridSearchCV(estimator=model_class, param_grid=param_grid_class, cv=3, n_jobs=-1)
grid_result_class = grid_class.fit(X_train_concat[:, :, np.newaxis], y_train_cat)

# Afisarea celor mai buni parametri pentru clasificarea comportamentului
print(f"Best parameters for Behaviour classification model: {grid_result_class.best_params_}")

# Reantrenarea modelului pentru clasificarea comportamentului cu cei mai buni parametri
best_units_class = grid_result_class.best_params_['units']
best_dropout_class = grid_result_class.best_params_['dropout_rate']
best_batch_size_class = grid_result_class.best_params_['batch_size']
best_epochs_class = grid_result_class.best_params_['epochs']

model_class = Sequential([
    LSTM(units=best_units_class, return_sequences=True, input_shape=(X_train_concat.shape[1], 1)),
    Dropout(best_dropout_class),
    LSTM(units=best_units_class, return_sequences=True),
    Dropout(best_dropout_class),
    LSTM(units=best_units_class),
    Dropout(best_dropout_class),
    Dense(3, activation='softmax')
])
model_class.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
model_class.fit(X_train_concat[:, :, np.newaxis], y_train_cat, epochs=best_epochs_class, batch_size=best_batch_size_class, validation_split=0.2)

# Evaluarea modelelor

mae = mean_absolute_error(y_test['Overall_%Modif'], y_pred_overall_test)
rmse = np.sqrt(mean_squared_error(y_test['Overall_%Modif'], y_pred_overall_test))
print(f"Mean Absolute Error pentru Overall_%Modif LSTM: {mae}")
print(f"Root Mean Squared Error pentru Overall_%Modif LSTM: {rmse}")

y_pred = model_class.predict(X_test_concat[:, :, np.newaxis])
y_pred_classes = np.argmax(y_pred, axis=1)
accuracy = accuracy_score(y_test['Behaviour'], y_pred_classes)
conf_matrix = confusion_matrix(y_test['Behaviour'], y_pred_classes)

print("Acuratetea modelului LSTM:", accuracy)
print("Matricea de confuzie:\n", conf_matrix)

# Afisarea graficului de evolutie a erorilor MAE si RMSE
train_loss = history_overall.history['loss']
val_loss = history_overall.history['val_loss']
train_mae = history_overall.history['mae']
val_mae = history_overall.history['val_mae']

plt.figure(figsize=(14, 6))
plt.plot(train_mae, label='Training MAE', color='blue')
plt.plot(np.sqrt(train_loss), label='Training RMSE', color='orange')
plt.title('Evoluția erorilor MAE și RMSE pe parcursul antrenamentului')
plt.xlabel('Epoch')
plt.ylabel('Error')
plt.legend()
plt.grid(True)
plt.show()

# Afisarea matricei de confuzie
plt.figure(figsize=(8, 6))
sns.heatmap(conf_matrix, annot=True, cmap='Blues', fmt='d', linewidths=0.5)
plt.title('Matricea de confuzie LSTM')
plt.xlabel('Predicted Label')
plt.ylabel('True Label')
plt.show()
