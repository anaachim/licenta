import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import datetime
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.metrics import confusion_matrix, mean_absolute_error, mean_squared_error, accuracy_score
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
from keras.wrappers.scikit_learn import KerasRegressor, KerasClassifier
from keras.callbacks import Callback

# Incarcarea setului de date
df = pd.read_csv("Stock_Market_Data.csv")  

# Eliminarea duplicatelor
df = df.drop_duplicates()

# Tratarea valorilor lipsa
df = df.fillna(method='ffill')  

# Transformarea coloanei Date in tip de date datetime
def StrToDateTime(s):
    split = s.split('/')
    monthDf, dayDf, yearDf = int(split[0]), int(split[1]), int(split[2])
    return datetime.datetime(year=yearDf, month=monthDf, day=dayDf)

df['Date'] = df['Date'].apply(StrToDateTime)

# Setarea coloanei Date ca index
df.index = df.pop('Date')

# Transformarea categoriilor in numere intregi
label_mapping_behaviour = {'Growth': 1, 'Decline': -1, 'Stagnation': 0}
df['Behaviour'] = df['Behaviour'].map(label_mapping_behaviour)

label_mapping_season = {'Winter': 3, 'Spring': 2, 'Summer': 0, 'Autumn': 1}
df['Season'] = df['Season'].map(label_mapping_season)

# Normalizarea datelor 
scaler = StandardScaler()
data_scaled = scaler.fit_transform(df.iloc[:, :-1]) 

# Impartirea setului de date in set de antrenament si set de testare
X_train, X_test, y_train, y_test = train_test_split(data_scaled, df[['Overall_%Modif', 'Behaviour']], 
                                                    test_size=0.2, random_state=42)

# Transformarea etichetelor pentru variabila 'Behaviour'
le = LabelEncoder()
y_train['Behaviour'] = le.fit_transform(y_train['Behaviour'])
y_test['Behaviour'] = le.transform(y_test['Behaviour'])

# Convertirea etichetelor in forma one-hot pentru variabila 'Behaviour'
y_train_cat = to_categorical(y_train['Behaviour'])
y_test_cat = to_categorical(y_test['Behaviour'])

# Callback pentru stocarea valorilor RMSE la fiecare epocă
class RMSECallback(Callback):
    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        y_pred_train = self.model.predict(X_train, verbose=0)
        y_pred_val = self.model.predict(X_test, verbose=0)
        train_rmse.append(np.sqrt(mean_squared_error(y_train['Overall_%Modif'], y_pred_train)))
        val_rmse.append(np.sqrt(mean_squared_error(y_test['Overall_%Modif'], y_pred_val)))

# Functia pentru crearea modelului MLP pentru Overall_%Modif
def create_model_overall(optimizer='sgd', dropout_rate=0.2):
    model = Sequential()
    model.add(Dense(64, input_dim=X_train.shape[1], activation='relu'))
    model.add(Dropout(dropout_rate))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(dropout_rate))
    model.add(Dense(1, activation='linear')) 
    model.compile(optimizer=optimizer, loss='mean_absolute_error')
    return model

# Definim și antrenăm modelul fără GridSearchCV pentru a putea obține istoricul antrenamentului
train_rmse = []
val_rmse = []

model_overall = create_model_overall(optimizer='sgd', dropout_rate=0.2)

# Antrenăm modelul și stocăm istoricul
history_overall = model_overall.fit(X_train, y_train['Overall_%Modif'], epochs=100, batch_size=32, 
                                    validation_split=0.2, verbose=0, callbacks=[RMSECallback()])

# Evaluarea modelului de regresie
y_pred_overall = model_overall.predict(X_test)
mae_overall = mean_absolute_error(y_test['Overall_%Modif'], y_pred_overall)
rmse_overall = np.sqrt(mean_squared_error(y_test['Overall_%Modif'], y_pred_overall))
print("Mean Absolute Error pentru Overall_%Modif MLP:", mae_overall)
print("Root Mean Squared Error pentru Overall_%Modif MLP:", rmse_overall)


# Functia pentru crearea modelului MLP pentru Behaviour
def create_model_behaviour(optimizer='sgd', dropout_rate=0.2):
    model = Sequential()
    model.add(Dense(64, input_dim=X_train.shape[1] + 1, activation='relu'))  
    model.add(Dropout(dropout_rate))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(dropout_rate))
    model.add(Dense(3, activation='softmax'))  # 3 clase pentru 'Behaviour'
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
    return model

# Crearea modelului KerasClassifier
model_behaviour = KerasClassifier(build_fn=create_model_behaviour, verbose=0)

# Definirea grilei de hiperparametri
param_grid_behaviour = {
    'batch_size': [16, 32, 64],
    'epochs': [50, 100, 150],
    'optimizer': ['sgd', 'adam'],
    'dropout_rate': [0.1, 0.2, 0.3]
}

# Crearea GridSearchCV pentru Behaviour
grid_behaviour = GridSearchCV(estimator=model_behaviour, param_grid=param_grid_behaviour, n_jobs=-1, cv=3)

# Antrenarea modelului folosind GridSearchCV
grid_result_behaviour = grid_behaviour.fit(np.hstack((X_train, y_train['Overall_%Modif'].values.reshape(-1, 1))), 
                                           y_train_cat)

# Afisarea celor mai buni parametri
print("Best params for Behaviour:", grid_result_behaviour.best_params_)

# Evaluarea modelului de clasificare
accuracy_behaviour = accuracy_score(np.argmax(y_test_cat, axis=1), 
                                    grid_result_behaviour.best_estimator_.predict(np.hstack((
                                        X_test,y_pred_overall.reshape(-1, 1)))))
conf_matrix_behaviour = confusion_matrix(np.argmax(y_test_cat, axis=1),
                                          grid_result_behaviour.best_estimator_.predict(np.hstack((
                                              X_test, y_pred_overall.reshape(-1, 1)))))
print("Acuratetea modelului MLP:", accuracy_behaviour)
print("Matricea de confuzie MLP:\n", conf_matrix_behaviour)

# Creare DataFrame cu datele reale si cele prezise pentru Overall_%Modif
df_comparison = pd.DataFrame({'Real': y_test['Overall_%Modif'].values, 'Predicted': y_pred_overall.flatten()})

# Salvare in fisier CSV
df_comparison.to_csv("comparison_procentual_changes.csv", index=False)

# Afisarea graficului erorilor pentru modelul de regresie folosind istoricul
plt.figure(figsize=(10, 6))
plt.plot(history_overall.history['loss'], label='Mean Absolute Error (MAE)')
plt.plot(train_rmse, label='Root Mean Squared Error (RMSE)')
plt.title('Evoluția erorilor pe parcursul antrenării')
plt.xlabel('Epoch')
plt.ylabel('Error')
plt.legend()
plt.grid(True)
plt.show()


# Afisare comparativa intre datele reale si cele prezise pentru Behaviour
plt.figure(figsize=(8, 6))
sns.heatmap(conf_matrix_behaviour, annot=True, cmap='Oranges', fmt='d', linewidths=0.5)
plt.title('Matricea de confuzie MLP')
plt.xlabel('Predicted Label')
plt.ylabel('True Label')
plt.show()
