import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import datetime
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.metrics import accuracy_score, confusion_matrix, mean_squared_error
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical

# Incarcarea setului de date
df = pd.read_csv("Stock_Market_Data.csv")

# Eliminarea duplicatelor
df = df.drop_duplicates()

# Tratarea valorilor lipsa
df = df.fillna(method='ffill')

# Transformarea coloanei Date in tip de date datetime
def StrToDateTime(s):
    split = s.split('/')
    month, day, year = int(split[0]), int(split[1]), int(split[2])
    return datetime.datetime(year=year, month=month, day=day)

df['Date'] = df['Date'].apply(StrToDateTime)

# Setarea coloanei Date ca index
df.index = df.pop('Date')

# Transformarea categoriilor in numere intregi
label_mapping_behaviour = {'Growth': 1, 'Decline': -1, 'Stagnation': 0}
df['Behaviour'] = df['Behaviour'].map(label_mapping_behaviour)

label_mapping_season = {'Winter': 3, 'Spring': 2, 'Summer': 0, 'Autumn': 1}
df['Season'] = df['Season'].map(label_mapping_season)

# Normalizarea datelor 
scaler = StandardScaler()
data_scaled = scaler.fit_transform(df.iloc[:, :-1])

# Impartirea setului de date in set de antrenament si set de testare
X_train, X_test, y_train, y_test = train_test_split(data_scaled, df[['Overall_%Modif', 'Behaviour']], 
                                                    test_size=0.2, random_state=42)

# Transformarea etichetelor pentru variabila 'Behaviour'
le = LabelEncoder()
y_train['Behaviour'] = le.fit_transform(y_train['Behaviour'])
y_test['Behaviour'] = le.transform(y_test['Behaviour'])

# Convertirea etichetelor in forma one-hot pentru variabila 'Behaviour'
y_train_cat = to_categorical(y_train['Behaviour'])
y_test_cat = to_categorical(y_test['Behaviour'])

# Functia pentru crearea modelului MLP pentru Overall_%Modif
def create_model_overall():
    model = Sequential()
    model.add(Dense(64, input_dim=X_train.shape[1], activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(1, activation='linear')) 
    model.compile(optimizer='sgd', loss='mean_absolute_error')
    return model

# Functia pentru crearea modelului MLP pentru Behaviour
def create_model_behaviour():
    model = Sequential()
    model.add(Dense(64, input_dim=X_train.shape[1] + 1, activation='relu'))  
    model.add(Dropout(0.2))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(3, activation='softmax'))  # 3 clase pentru 'Behaviour'
    model.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])
    return model

# Crearea si antrenarea modelului de baza pentru Overall_%Modif
model_overall_base = create_model_overall()
history_overall_base = model_overall_base.fit(X_train, y_train['Overall_%Modif'], epochs=50, batch_size=32, verbose=1)

# Prezicerea Overall_%Modif pentru setul de testare cu modelul de baza
y_pred_overall_base = model_overall_base.predict(X_test)

# Evaluarea modelului pentru Overall_%Modif
mae_overall_base = model_overall_base.evaluate(X_test, y_test['Overall_%Modif'], verbose=0)
print("Mean Absolute Error pentru Overall_%Modif MLP:", mae_overall_base)

# Crearea si antrenarea modelului de baza pentru Behaviour
model_behaviour_base = create_model_behaviour()
history_behaviour_base = model_behaviour_base.fit(np.hstack((X_train, y_train['Overall_%Modif'].values.reshape(-1, 1))), y_train_cat, epochs=50, batch_size=32, verbose=1)

rmse_overall_base = np.sqrt(mean_squared_error(y_test['Overall_%Modif'], y_pred_overall_base))
print("Root Mean Squared Error pentru Overall_%Modif MLP:", rmse_overall_base)

# Evaluarea modelului pentru Behaviour
accuracy_behaviour_base = model_behaviour_base.evaluate(np.hstack((X_test, y_pred_overall_base.reshape(-1, 1))), y_test_cat, verbose=0)[1]
print("Acuratetea modelului MLP:", accuracy_behaviour_base)

# Prezicerea clasei pentru Behaviour
y_pred_classes_behaviour_base = np.argmax(model_behaviour_base.predict(np.hstack((X_test, y_pred_overall_base.reshape(-1, 1)))), axis=1)
conf_matrix_behaviour_base = confusion_matrix(np.argmax(y_test_cat, axis=1), y_pred_classes_behaviour_base)
print("Matricea de confuzie MLP:\n", conf_matrix_behaviour_base)

# Creare DataFrame cu datele reale si cele prezise pentru Overall_%Modif
df_comparison = pd.DataFrame({'Real': y_test['Overall_%Modif'].values, 'Predicted': y_pred_overall_base.flatten()})

# Salvare in fisier CSV
df_comparison.to_csv("comparison_procentual_changes.csv", index=False)

# Afisarea graficului erorilor pentru modelul de regresie
plt.figure(figsize=(10, 6))
plt.plot(history_overall_base.history['loss'], label='Mean Absolute Error (MAE)')
plt.plot(np.sqrt(history_overall_base.history['loss']), label='Root Mean Squared Error (RMSE)')
plt.title('Evoluția erorilor pe parcursul antrenării')
plt.xlabel('Epoch')
plt.ylabel('Error')
plt.legend()
plt.grid(True)
plt.show()

# Afisare comparativa intre datele reale si cele prezise pentru Behaviour
plt.figure(figsize=(8, 6))
sns.heatmap(conf_matrix_behaviour_base, annot=True, cmap='Oranges', fmt='d', linewidths=0.5)
plt.title('Matricea de confuzie MLP')
plt.xlabel('Predicted Label')
plt.ylabel('True Label')
plt.show()
