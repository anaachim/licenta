import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.metrics import accuracy_score, confusion_matrix, mean_absolute_error, mean_squared_error
from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout
from keras.utils import to_categorical

# Incarcarea setului de date
df = pd.read_csv("Stock_Market_Data.csv")

# Eliminarea duplicatelor
df = df.drop_duplicates()

# Tratarea valorilor lipsa
df = df.fillna(method='ffill')

# Transformarea categoriilor in numere intregi
label_mapping_behaviour = {'Growth': 1, 'Decline': -1, 'Stagnation': 0}
df['Behaviour'] = df['Behaviour'].map(label_mapping_behaviour)

label_mapping_season = {'Winter': 3, 'Spring': 2, 'Summer': 0, 'Autumn': 1}
df['Season'] = df['Season'].map(label_mapping_season)

# Normalizarea datelor numerice
scaler = StandardScaler()
data_scaled = scaler.fit_transform(df.iloc[:, 1:-2])

# Impartirea setului de date in set de antrenament si set de testare
X_train, X_test, y_train, y_test = train_test_split(data_scaled, df[['Overall_%Modif', 'Behaviour']], test_size=0.2, random_state=42)

# Transformarea etichetelor pentru variabila 'Behaviour'
le = LabelEncoder()
y_train['Behaviour'] = le.fit_transform(y_train['Behaviour'])
y_test['Behaviour'] = le.transform(y_test['Behaviour'])

# Convertirea etichetelor in forma one-hot
y_train_cat = to_categorical(y_train['Behaviour'])
y_test_cat = to_categorical(y_test['Behaviour'])

# Definirea functiei care creeaza modelul pentru predictia 'Overall_%Modif'
def create_model():
    model = Sequential([
        LSTM(units=25, return_sequences=True, input_shape=(X_train.shape[1], 1)),
        Dropout(0.2),
        LSTM(units=25, return_sequences=True),
        Dropout(0.2),
        LSTM(units=25),
        Dropout(0.2),
        Dense(1, activation='linear')
    ])
    model.compile(optimizer='adam', loss='mean_squared_error', metrics=['mae'])
    return model

# Crearea si antrenarea modelului pentru predictia 'Overall_%Modif'
model_overall = create_model()
history_overall = model_overall.fit(X_train[:, :, np.newaxis], y_train['Overall_%Modif'], epochs=50, batch_size=32, validation_split=0.2)

# Prezicerea 'Overall_%Modif' pentru seturile de antrenament si testare
y_pred_overall_train = model_overall.predict(X_train[:, :, np.newaxis])
y_pred_overall_test = model_overall.predict(X_test[:, :, np.newaxis])

# Verificarea reshaping-ului
if len(y_pred_overall_train.shape) == 1:
    y_pred_overall_train = y_pred_overall_train.reshape(-1, 1)
if len(y_pred_overall_test.shape) == 1:
    y_pred_overall_test = y_pred_overall_test.reshape(-1, 1)

# Verificarea concatenarii
X_train_concat = np.concatenate((X_train, y_pred_overall_train), axis=1)
X_test_concat = np.concatenate((X_test, y_pred_overall_test), axis=1)

# Definirea functiei care creeaza modelul pentru clasificarea comportamentului
def create_classification_model():
    model = Sequential([
        LSTM(units=25, return_sequences=True, input_shape=(X_train_concat.shape[1], 1)),
        Dropout(0.2),
        LSTM(units=25, return_sequences=True),
        Dropout(0.2),
        LSTM(units=25),
        Dropout(0.2),
        Dense(3, activation='softmax')
    ])
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    return model

# Crearea si antrenarea modelului pentru clasificarea comportamentului
model_class = create_classification_model()
model_class.fit(X_train_concat[:, :, np.newaxis], y_train_cat, epochs=50, batch_size=32, validation_split=0.2)

# Evaluarea modelelor
mae = mean_absolute_error(y_test['Overall_%Modif'], y_pred_overall_test)
rmse = np.sqrt(mean_squared_error(y_test['Overall_%Modif'], y_pred_overall_test))
print(f"Mean Absolute Error pentru Overall_%Modif LSTM: {mae}")
print(f"Root Mean Squared Error pentru Overall_%Modif LSTM: {rmse}")

# Prezicerea comportamentului pentru setul de testare
y_pred = model_class.predict(X_test_concat[:, :, np.newaxis])
y_pred_classes = np.argmax(y_pred, axis=1)

# Calcularea acuratetii si a matricei de confuzie
accuracy = accuracy_score(y_test['Behaviour'], y_pred_classes)
conf_matrix = confusion_matrix(y_test['Behaviour'], y_pred_classes)

print("Acuratetea modelului LSTM:", accuracy)
print("Matricea de confuzie:\n", conf_matrix)

# Afisarea graficului de evolutie a erorilor MAE si RMSE
train_loss = history_overall.history['loss']
val_loss = history_overall.history['val_loss']
train_mae = history_overall.history['mae']
val_mae = history_overall.history['val_mae']

plt.figure(figsize=(14, 6))
plt.plot(train_mae, label='Training MAE', color='blue')
plt.plot(np.sqrt(train_loss), label='Training RMSE', color='orange')
plt.title('Evoluția erorilor MAE și RMSE pe parcursul antrenamentului')
plt.xlabel('Epoch')
plt.ylabel('Error')
plt.legend()
plt.grid(True)
plt.show()

# Afisarea matricei de confuzie
plt.figure(figsize=(8, 6))
sns.heatmap(conf_matrix, annot=True, cmap='Blues', fmt='d', linewidths=0.5)
plt.title('Matricea de confuzie LSTM')
plt.xlabel('Predicted Label')
plt.ylabel('True Label')
plt.show()
